#
# Cookbook Name:: gryphon_development_cookbook/
# Recipe:: default
#
# Copyright (C) 2014 State News
#
# All rights reserved - Do Not Redistribute
#

client_code = node['gryphon_development']['client_code']
client_code_clean = client_code.sub(/-/, '_')

include_recipe "apt"

package "make"
package "autoconf"
package "automake"
package "apache2"
package "php5"
package "ghostscript"
package "mysql-client"
package "libjpeg-turbo8"
package "libjpeg8"
package "php-apc"
package "php-pear"
package "php5-curl"
package "php5-dev"
package "php5-gd"
package "php5-mcrypt"
package "php5-mysql"
package "php5-sqlite"
package "php5-xsl"
package "php5-tidy"
package "php5-curl"

php_pear "mongo" do
    action :install
end

include_recipe "git"

ssh_known_hosts_entry 'github.com'
ssh_known_hosts_entry 'bitbucket.org'

node.default['apache']['listen_ports'] = ['80']

# Make sure apache modes are enabled and roll apache (done automatically)
include_recipe "apache2::mod_ssl"
include_recipe "apache2::mod_rewrite"
include_recipe "apache2::mod_deflate"
include_recipe "apache2::mod_headers"

node.set['mysql']['server_root_password'] = 'thisisapassword'
node.set['mysql']['allow_remote_root'] = true

include_recipe 'mysql::server'

# Make sure the shared resources are setup
directory "/usr/local/share/snworks" do
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

cookbook_file "/usr/local/share/snworks/SNworks.php" do
    source 'SNworks.php'
    mode '0644'
    group 'root'
    owner 'root'
    action :create
end

controller_files = ['index.php', 'bootstrap.php', 'foundry.config.php']
controller_files.each do |f|
    cookbook_file "/srv/www/#{client_code}/current/www/#{f}" do
        source f
        mode '0644'
        group 'vagrant'
        owner 'vagrant'
        action :create
    end
end

template "Deployment.php" do
    path "/srv/www/#{client_code}/current/Deployment.php"
    source "Deployment.php.erb"
    owner 'root'
    group 'root'
    mode '0644'
    variables(
        :client_code    => client_code,
        :app_env        => 'stage'
    )
end

# Finally, deploy Gryphon, yo
# directory "/usr/local/share/gryphon" do
#     owner 'vagrant'
#     group 'vagrant'
#     mode '0755'
#     action :create
# end

directory "/etc/apache2/sites-enabled" do
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    recursive true
end

directory "/usr/local/share/snworks/media" do
    owner 'www-data'
    group 'www-data'
    mode '0777'
    action :create
end
directory "/usr/local/share/snworks/private" do
    owner 'www-data'
    group 'www-data'
    mode '0777'
    action :create
end
link "/srv/www/#{client_code}/current/www/media" do
    to "/usr/local/share/snworks/media"
end
link "/srv/www/#{client_code}/current/private" do
    to "/usr/local/share/snworks/private"
end
link "/srv/www/#{client_code}/current/www/assets" do
    to "/srv/www/#{client_code}/current/assets"
end
link "/srv/www/#{client_code}/current/www/gAssets" do
    to "/usr/local/share/gryphon/www/gAssets"
end
link "/srv/www/#{client_code}/current/www/gryphon-assets" do
    to "/usr/local/share/gryphon/www/gryphon-assets"
end

execute "chown-site-media" do
    command "chown -R www-data:www-data /srv/www/#{client_code}/current/www/media"
    user "root"
    action :run
end
execute "chown-site-private" do
    command "chown -R www-data:www-data /srv/www/#{client_code}/current/private"
    user "root"
    action :run
end
file "/etc/apache2/sites-enabled/#{client_code}.conf" do
    owner 'root'
    group 'root'
    mode 0644
    content lazy {
        ::File.open("/srv/www/#{client_code}/current/config/httpd.dev.conf").read
    }
    action :create
end

cookbook_file "/etc/php5/conf.d/gryphon.ini" do
    source "gryphon.ini"
    mode '0644'
    group 'root'
    owner 'root'
    action :create
end

cookbook_file "/etc/apache2/conf-enabled/gryphon.conf" do
    source "gryphon.conf"
    mode '0644'
    group 'root'
    owner 'root'
    action :create
end


# Restart Apache
httpd_service 'default' do
    action :restart
end

execute "mysql-create-db" do
  command "mysql -u root -pthisisapassword -e 'create database if not exists #{client_code_clean}_stage character set utf8 collate utf8_general_ci'"
end
execute "mysql-update-db" do
  command "mysql -u root -pthisisapassword #{client_code_clean}_stage < /vagrant/support/#{client_code_clean}_stage.sql"
end
