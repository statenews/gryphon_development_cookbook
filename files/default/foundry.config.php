<?php

date_default_timezone_set(GRYPHON_TIMEZONE);

foundry_init(array(
	'namespace'					=> 'gryphon',
	'config-path'				=> GRYPHON_SYS_PATH.'/config',
));
