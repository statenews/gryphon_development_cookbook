<?php

class SNworks {

    private static $databases = array(
        'alpha' => array(
            'host'  => 'localhost',
            'user'  => 'root',
            'pass'  => 'thisisapassword'
        ),
        'beta' => array(
            'host'  => 'localhost',
            'user'  => 'root',
            'pass'  => 'thisisapassword'
        )
    );

    public static function database($serv) {
        return self::$databases[$serv];
    }

    public static function env($env, $y, $n) {
        if( GRYPHON_ENV == $env ) {
            return $y;
        }

        return $n;
    }
    
    public static function key($k) {
        return false;
    }
}
