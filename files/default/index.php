<?php

// don't pollute root namespaces
call_user_func(function() {
    $d = dirname(__FILE__);
    $r = realpath($d.'/..');

    require_once $r.'/Deployment.php';
    require_once $r.'/GryphonEnv.php';

    foreach( $_env as $k=>$v ) {
        $k = 'GRYPHON_'.strtoupper($k);
        define($k, $v);
    }
});

include_once(GRYPHON_APP_PATH.'/www/index.php');
