name             'gryphon_development_cookbook'
maintainer       'Mike Joseph'
maintainer_email 'mike@statenews.com'
license          'All rights reserved'
description      'Installs/Configures gryphon_development_cookbook'
long_description 'Installs/Configures gryphon_development_cookbook'
version          '0.1.0'

depends 'apt', '~> 2.6.0'
depends 'varnish', '~> 0.9.18'
depends 'apache2', '~> 2.0.0'   # apache2 cookbook to configure Apache and modules
depends 'httpd', '~> 0.1.5'     # httpd cookbook to manage it as a service (e.g. restarts)
depends 'git', '~> 4.0.2'
depends 'ssh_known_hosts', '~> 1.3.2'
depends 'php', '~> 1.5.0'
depends 'composer', '~> 1.0.5'
depends 'mysql', '~> 5.5.4'
